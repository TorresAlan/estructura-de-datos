
module MultiSet1 (
    MultiSet,
    emptyMS,
    addMS,
    ocurrencesMS,
    unionMS,
    intersectionMS,
    multiSetToList
) where

import Map3

data MultiSet a = MST (Map a Int) 
                    deriving Show





--Propósito: denota un multiconjunto vacío.
emptyMS :: MultiSet a
emptyMS = MST [] []

--Propósito: dados un elemento y un multiconjunto, agrega una ocurrencia de ese elemento al
--multiconjunto.
addMS :: Ord a => a -> MultiSet a -> MultiSet a
addMS = undefined

--Propósito: dados un elemento y un multiconjunto indica la cantidad de apariciones de ese
--elemento en el multiconjunto.
ocurrencesMS :: Ord a => a -> MultiSet a -> Int
ocurrencesMS = undefined

--Propósito: dados dos multiconjuntos devuelve un multiconjunto con todos los elementos de
--ambos multiconjuntos.
unionMS :: Ord a => MultiSet a -> MultiSet a -> MultiSet a (opcional)
unionMS = undefined

--Propósito: dados dos multiconjuntos devuelve el multiconjunto de elementos que ambos
--multiconjuntos tienen en común.
intersectionMS :: Ord a => MultiSet a -> MultiSet a -> MultiSet a (opcional)
intersectionMS = undefined

--Propósito: dado un multiconjunto devuelve una lista con todos los elementos del conjunto y
--su cantidad de ocurrencias.
multiSetToList :: MultiSet a -> [(a, Int)]
multiSetToList = undefined