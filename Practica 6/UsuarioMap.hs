import Map3

-- Interfaz
-- emptyM :: Map k v
-- assocM :: Eq k => k -> v -> Map k v -> Map k v
-- lookupM :: Eq k => k -> Map k v -> Maybe v
-- deleteM :: Eq k => k -> Map k v -> Map k v
-- keys :: Map k v -> [k]

valores :: Eq k => Map k v -> [v]
valores m = obtenerValores (keys m) m

obtenerValores :: Eq k => [k] -> Map k v -> [v]
obtenerValores [] m     = []
obtenerValores (k:ks) m = 
	valor (lookupM k m) : obtenerValores ks m

-- data Maybe a = Nothing | Just a

-- Parcial cuando es Nothing
valor :: Maybe v -> v
valor Nothing = error "no se obtener un valor"
valor (Just x) = x

claveExiste :: Eq k => k -> Map k v -> Bool
claveExiste k m = esJust (lookupM k m)

esJust (Just x) = True
esJust Nothing  = False

map1 :: Map String Int
map1 =
	assocM "fede" 123 $
	assocM "ale" 456 $
	assocM "cristian" 789 $
	emptyM

map2 :: Map String Int
map2 = assocM "ale" 999 $ map1

numerosDelLoto :: Map Int Bool
numerosDelLoto = 
	assocM 23 True $
	assocM 44 True $
	assocM 55 True $
	assocM 70 True $
	assocM 99 False $
	assocM 77 False $
	assocM 22 False $
	assocM 33 False $
	emptyM

esNumeroGanador :: Int -> Map Int Bool -> Bool
esNumeroGanador n m = 
	esJust (lookupM n m) && valor (lookupM n m)

	-- if esJust (lookupM n m)
	--    then valor (lookupM n m)
	--    else False

--  otra opcion:
-- 	aparece (lookupM n m)

-- aparece :: Maybe Bool -> Bool
-- aparece Nothing  = False
-- aparece (Just b) = b



--Propósito: obtiene los valores asociados a cada clave del map.
valuesM :: Eq k => Map k v -> [Maybe v]
valuesM mkv = valuesOfK (keys mkv) mkv

valuesOfK :: Eq k => [k] -> Map k v -> [Maybe v]
valuesOfK [] mkv = []
valuesOfK (k:ks) mkv = lookupM k mkv : (valuesOfK ks mkv)


--Propósito: indica si en el map se encuenrtan todas las claves dadas.
todasAsociadas :: Eq k => [k] -> Map k v -> Bool
todasAsociadas [] _ = error "No me diste ninguna clave"
todasAsociadas k mkv = todasAsociadas' k (keys mkv) 

todasAsociadas' :: Eq k => [k] -> [k] -> Bool
todasAsociadas' [] ks2 = True
todasAsociadas' (k:ks) ks2 = 
	if elem k ks2
		then True && todasAsociadas' ks ks2
		else False

--Propósito: convierte una lista de pares clave valor en un map.
listToMap :: Eq k => [(k, v)] -> Map k v
listToMap lkv = listToMap' lkv emptyM

listToMap' :: Eq k => [(k, v)] -> Map k v -> Map k v
listToMap' [] mkv = mkv
listToMap' (x:xs) mkv = listToMap' xs (assocM (fst x) (snd x) mkv )

--Propósito: convierte un map en una lista de pares clave valor.
mapToList :: Eq k => Map k v -> [(k, v)]
mapToList mkv = mapToList' (keys mkv) mkv

mapToList' :: Eq k => [k] -> Map k v -> [(k, v)]
mapToList' [] mkv = []
mapToList' (k:ks) mkv = (k, valor (lookupM k mkv)) : (mapToList' ks mkv)


--Propósito: dada una lista de pares clave valor, agrupa los valores de los pares que compartan
--la misma clave.
agruparEq :: Eq k => [(k, v)] -> Map k [v]
agruparEq = undefined


--Propósito: dada una lista de claves de tipo k y un mapa que va de k a int, le suma uno a
--cada número asociado con dichas claves.
incrementar :: Eq k => [k] -> Map k Int -> Map k Int
incrementar = undefined

--Propósito: dado dos maps se agregan las claves y valores del primer map en el segundo. Si
--una clave del primero existe en el segundo, es reemplazada por la del primero.
mergeMaps:: Eq k => Map k v -> Map k v -> Map k v
mergeMaps = undefined
