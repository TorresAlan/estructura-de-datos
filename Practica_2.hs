--1. Recursión sobre listas

--Defina las siguientes funciones utilizando recursión estructural sobre listas, salvo que se indique
--lo contrario:

--1)Dada una lista de enteros devuelve la suma de todos sus elementos.
sumatoria :: [Int] -> Int
sumatoria [] = 0
sumatoria (x:xs) = x + sumatoria xs

--2)Dada una lista de elementos de algún tipo devuelve el largo de esa lista, es decir, la cantidad
--de elementos que posee.
longitud :: [a] -> Int
longitud [] = 0
longitud (x:xs) = 1 + longitud xs

--3)Dada una lista de enteros, devuelve la lista de los sucesores de cada entero.
sucesores :: [Int] -> [Int]
sucesores [] = []
sucesores (x:xs) = x + 1 : sucesores xs

--4)Dada una lista de booleanos devuelve True si todos sus elementos son True.
conjuncion :: [Bool] -> Bool
conjuncion [] = True
conjuncion (x:xs) = x && conjuncion xs

--5)Dada una lista de booleanos devuelve True si alguno de sus elementos es True.
disyuncion :: [Bool] -> Bool
disyuncion [] = False
disyuncion (x:xs) = x || disyuncion xs

--6)Dada una lista de listas, devuelve una única lista con todos sus elementos.
aplanar :: [[a]] -> [a]
aplanar [] = []
aplanar (x:xs) = x ++ aplanar xs

--7)Dados un elemento e y una lista xs devuelve True si existe un elemento en xs que sea igual
--a e.
pertenece :: Eq a => a -> [a] -> Bool
pertenece e [] = False
pertenece e (x:xs) = x == e || pertenece e xs 

--8)Dados un elemento e y una lista xs cuenta la cantidad de apariciones de e en xs.
apariciones :: Eq a => a -> [a] -> Int
apariciones e [] = 0
apariciones e (x:xs) = if e == x 
                            then 1 + apariciones e xs
                            else apariciones e xs

--9)Dados un número n y una lista xs, devuelve todos los elementos de xs que son menores a n.
losMenoresA :: Int -> [Int] -> [Int]
losMenoresA n [] = []
losMenoresA n (x:xs) = if x < n 
                            then x : losMenoresA n xs
                            else losMenoresA n xs

--10)Dados un número n y una lista de listas, devuelve la lista de aquellas listas que tienen más
--de n elementos.
lasDeLongitudMayorA :: Int -> [[a]] -> [[a]]
lasDeLongitudMayorA n [] = []
lasDeLongitudMayorA n (x:xs) = if longitud x > n 
                                    then x : lasDeLongitudMayorA n xs
                                    else lasDeLongitudMayorA n xs

--11)Dados una lista y un elemento, devuelve una lista con ese elemento agregado al final de la
--lista.
agregarAlFinal :: [a] -> a -> [a]
agregarAlFinal [] e = e : []
agregarAlFinal (x:xs) e = x : agregarAlFinal xs e

--12)Dadas dos listas devuelve la lista con todos los elementos de la primera lista y todos los
--elementos de la segunda a continuación. Definida en Haskell como ++.
concatenar :: [a] -> [a] -> [a]
concatenar l [] = l
concatenar l (x:xs) = concatenar (agregarAlFinal l x) xs

--13)Dada una lista devuelve la lista con los mismos elementos de atrás para adelante. Definida
--en Haskell como reverse.
reversa :: [a] -> [a]
reversa [] = []
reversa (x:xs) = agregarAlFinal (reversa xs) x 

--14)Dadas dos listas de enteros, devuelve una lista donde el elemento en la posición n es el
--máximo entre el elemento n de la primera lista y de la segunda lista, teniendo en cuenta que
--las listas no necesariamente tienen la misma longitud.
zipMaximos :: [Int] -> [Int] -> [Int]
zipMaximos [] _ = []
zipMaximos _ [] = []
zipMaximos (x:xs) (y:ys) = max x y : zipMaximos xs ys

--15)Dada una lista devuelve el mínimo
--Precondicion = necesito al menos un elemento en la lista
elMinimo :: Ord a => [a] -> a
elMinimo [] = error "la lista necesita al menos un elemento"
elMinimo [a] = a
elMinimo (x:y:xs) = elMinimo (min x y : xs)



--2. Recursión sobre números

--Defina las siguientes funciones utilizando recursión sobre números enteros, salvo que se indique
--lo contrario:

--1)Dado un número n se devuelve la multiplicación de este número y todos sus anteriores hasta
--llegar a 0. Si n es 0 devuelve 1. La función es parcial si n es negativo.
factorial :: Num a => a -> a
factorial 0 = 1
factorial n = n * factorial (n - 1)

--2)Dado un número n devuelve una lista cuyos elementos sean los números comprendidos entre
--n y 1 (incluidos). Si el número es inferior a 1, devuelve la lista vacía.
cuentaRegresiva :: Int -> [Int]
cuentaRegresiva 0 = []
cuentaRegresiva n = if n > 0 
                        then n : cuentaRegresiva (n - 1)
                        else cuentaRegresiva 0

--3)Dado un número n y un elemento e devuelve una lista en la que el elemento e repite n veces.
--Precondicion = n no debe ser un numero negativo
repetir :: Int -> a -> [a]
repetir 0 e = []
repetir n e = if n > 0 
                    then e : repetir (n - 1) e
                    else error "n no debe ser un numero negativo"

--4)Dados un número n y una lista xs, devuelve una lista con los n primeros elementos de xs.
--Si la lista es vacía, devuelve una lista vacía.
--Precondicion = n no debe ser un numero negativo
losPrimeros :: Int -> [a] -> [a]
losPrimeros n [] = []
losPrimeros 0 _ = []
losPrimeros n (x:xs) = if n > 0 
                        then x : losPrimeros (n - 1) xs
                        else error "n no debe ser un numero negativo"

--5)Dados un número n y una lista xs, devuelve una lista sin los primeros n elementos de lista
--recibida. Si n es cero, devuelve la lista completa.
--Precondicion = n no debe ser un numero negativo
sinLosPrimeros :: Int -> [a] -> [a]
sinLosPrimeros n [] = []
sinLosPrimeros 0 xs = xs
sinLosPrimeros n (x:xs) = sinLosPrimeros (n - 1) xs


--3. Registros

--1. Definir el tipo de dato Persona, como un nombre y la edad de la persona. Realizar las
--siguientes funciones:

data Persona = ConsP String Int
    deriving Show

pepe = ConsP "Pepe" 20
popo = ConsP "Popo" 30
pupu = ConsP "Pupu" 10

--Dados una edad y una lista de personas devuelve todas las personas que son mayores
--a esa edad.
mayoresA :: Int -> [Persona] -> [Persona]
mayoresA e [] = []
mayoresA e (x:xs) = if esMayor e x 
                        then x : mayoresA e xs
                        else mayoresA e xs

--Proposito: dada un edad e y una persona, devuelve True si la edad de la persona es mayor a e.
esMayor :: Int -> Persona -> Bool
esMayor e p = edad p > e

--Proposito: dada una persona devuelve su edad.
edad :: Persona -> Int
edad (ConsP nombre edad) = edad

--Dada una lista de personas devuelve el promedio de edad entre esas personas. 
--Precondición: la lista al menos posee una persona.
promedioEdad :: [Persona] -> Int
promedioEdad [] = error "la lista debe poseer al menos una persona"
promedioEdad xs = div (sumaEdad xs) (longitud xs)

--Proposito: Dada una lista de personas devuelve la suma de sus edades.
sumaEdad :: [Persona] -> Int
sumaEdad [] = 0
sumaEdad (x:xs) = edad x + sumaEdad xs

--Dada una lista de personas devuelve la persona más vieja de la lista. Precondición: la
--lista al menos posee una persona.
elMasViejo :: [Persona] -> Persona
elMasViejo [a] = a
elMasViejo (x:y:xs) = elMasViejo ((elViejo x y) : xs)

--Proposito: Dadas dos persona devuelve la de mayor edad
elViejo :: Persona -> Persona -> Persona
elViejo per1 per2 = if edad per1 > edad per2
                        then per1
                        else per2


--2.Modificaremos la representación de Entreador y Pokemon de la práctica anterior de la si-
--guiente manera:

data TipoDePokemon = Agua | Fuego | Planta
data Pokemon = ConsPokemon TipoDePokemon Int
data Entrenador = ConsEntrenador String [Pokemon]

charizard = ConsPokemon Fuego 20
venusaur = ConsPokemon Planta 15
blastoise = ConsPokemon Agua 17
charmander = ConsPokemon Fuego 5
squirtle = ConsPokemon Agua 6
rojo = ConsEntrenador "Rojo" [charizard, venusaur, blastoise]
verde = ConsEntrenador "Verde" [charmander, charmander, squirtle]

--Como puede observarse, ahora los entrenadores tienen una cantidad de Pokemon arbitraria.
--Definir en base a esa representación las siguientes funciones:

--Devuelve la cantidad de pokémon que posee el entrenador.
cantPokemones :: Entrenador -> Int
cantPokemones (ConsEntrenador nombre pokemones) = longitud pokemones

--Proposito: Dado un entrenador devuelve sus pokemones.
pokemonesDe :: Entrenador -> [Pokemon]
pokemonesDe (ConsEntrenador nombre pokemones) = pokemones

--Proposito: Dado un pokemon devuelve su tipo.
tipoDePokemon :: Pokemon -> TipoDePokemon
tipoDePokemon (ConsPokemon tipo e) = tipo

--Proposito: Dado dos tipos de pokemon, devuelve True si son iguales.
mismoTipo :: TipoDePokemon -> TipoDePokemon -> Bool
mismoTipo Fuego Fuego = True
mismoTipo Agua Agua = True
mismoTipo Planta Planta = True
mismoTipo _ _ = False

--Proposito: Dada un tipo y una lista de pokemones, devuelve los pokemones de ese tipo.
pokemonesDeTipo :: TipoDePokemon -> [Pokemon] -> [Pokemon]
pokemonesDeTipo tipo [] = []
pokemonesDeTipo tipo (x:xs) = if (mismoTipo tipo (tipoDePokemon x))
                                    then x : pokemonesDeTipo tipo xs
                                    else pokemonesDeTipo tipo xs 

--Devuelve la cantidad de pokémon de determinado tipo que posee el entrenador.
cantPokemonesDe :: TipoDePokemon -> Entrenador -> Int
cantPokemonesDe tipo entr = longitud (pokemonesDeTipo tipo (pokemonesDe entr))

--Dados dos entrenadores, indica la cantidad de Pokemon de cierto tipo, que le ganarían
--a los Pokemon del segundo entrenador.
losQueLeGanan :: TipoDePokemon -> Entrenador -> Entrenador -> Int
losQueLeGanan tipo entr1 entr2 = if (cantPokemonesDe (elTipoDebilA tipo) entr2) > 0
                                    then cantPokemonesDe tipo entr1
                                    else 0

--Proposito: Dado un tipo de pokemon, devuelve el tipo debil a este.
elTipoDebilA :: TipoDePokemon -> TipoDePokemon
elTipoDebilA Fuego = Planta
elTipoDebilA Planta = Agua
elTipoDebilA Agua = Fuego










