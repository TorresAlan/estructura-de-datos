module Investigacion
(
  Investigacion,
  comenzarInvestigacion,
  cantEvidenciaIngresada,
  evidenciaIngresada,
  nombresIngresados,
  casoCerrado,
  esSospechoso,
  posiblesInocentes,
  ingresarPersonas,
  ingresarEvidencia
)
where

import Map
import PriorityQueue

data Investigacion = ConsI (Map Nombre Persona)
                    (Map Evidencia [Nombre])
                    (PriorityQueue Persona)
                    Int

type Nombre = String
type Evidencia = String

---------------------------------------------------------------------------------------------------
--a) Invariantes de representacion:
--   *Las personas en el map de nombres y la PQ de personas son las mismas.
--   *No puede haber dos personas con el mismo nombre.
--   *No puede haber dos evidencias con el mismo nombre.
---------------------------------------------------------------------------------------------------

--b)Propósito: crea una investigación sin datos.
--  Eficiencia: O(1)
comenzarInvestigacion :: Investigacion
comenzarInvestigacion = ConsI emptyM emptyM emptyPQ 0

--c)Propósito: devuelve la cantidad de eviencia ingresada.
--  Eficiencia: O(1)
cantEvidenciaIngresada :: Investigacion -> Int
cantEvidenciaIngresada (ConsI per evi maxEv cantEv) = cantEv

--d)Propósito: devuelve la evidencia ingresada.
--  Eficiencia: O(N)
evidenciaIngresada :: Investigacion -> [Evidencia]
evidenciaIngresada (ConsI per evi maxEv cantEv) = domM evi

--e)Propósito: devuelve los nombres de personas ingresadas.
--  Eficiencia: O(N)
nombresIngresados :: Investigacion -> [Nombre]
nombresIngresados (ConsI per evi maxEv cantEv) = domM per

--f)Propósito: indica si la investigación posee al menos una persona con 5 evidencias en su contra.
--  Eficiencia: O(1)
casoCerrado :: Investigacion -> Bool
casoCerrado (ConsI per evi maxEv cantEv) = (cantEvidencia (maxPQ maxEv)) >= 5

--g)Propósito: indica si esa persona tiene al menos una evidencia en su contra.
--  Nota: la persona puede no existir.
--  Eficiencia: O(log N)
esSospechoso :: Nombre -> Investigacion -> Bool
esSospechoso nom (ConsI per evi maxEv cantEv) =
    case lookupM nom per of
        (Just x) -> (cantEvidencia x) > 0
        Nothing -> False 

--h)Propósito: devuelve a las personas con cero evidencia en su contra.
--  Eficiencia: O(N log N)
posiblesInocentes :: Investigacion -> [Persona]
posiblesInocentes (ConsI per evi maxEv cantEv) = posiblesInocentes' maxEv

--  Eficiencia: O(N log N) ya que por cada elemento de la PQ hace una operacion log N (deleteMaxPQ)
posiblesInocentes' :: PriorityQueue Persona -> [Persona]
posiblesInocentes' pq = 
    if isEmptyPQ pq
        then []
        else if cantEvidencia (maxPQ pq) == 0
                then (maxPQ pq) : posiblesInocentes' (deleteMaxPQ pq)
                else posiblesInocentes' (deleteMaxPQ pq)

--i)Propósito: ingresa a personas nuevas a la investigación (mediante sus nombres), sin evidencia en su contra.
--  Precondición: las personas no existen en la investigación y no hay nombres repetidos.
--  Eficiencia: O(N log N)
ingresarPersonas :: [Nombre] -> Investigacion -> Investigacion
ingresarPersonas [] inv = inv
ingresarPersonas (n:ns) inv = ingresarPersonas ns (ingresarPersona n inv)

--  Eficiencia: O(log N)
ingresarPersona :: Nombre -> Investigacion -> Investigacion
ingresarPersona n (ConsI per evi maxEv cantEv) = ConsI (assocM n (crearP n) per)
                                                 evi
                                                 (insertPQ (crearP n) maxEv)
                                                 cantEv

--j)Propósito: asocia una evidencia a una persona dada.
--  Precondición: la evidencia aún no está asociada a esa persona.
--  Nota: la persona y la evidencia existen, pero NO están asociadas.
--  Eficiencia: O(N log N)
ingresarEvidencia :: Evidencia -> Nombre -> Investigacion -> Investigacion
ingresarEvidencia ev nom (ConsI per evi maxEv cantEv) = ConsI
                                                        (agregarEvPer ev nom per)
                                                        (agregarPerEv ev nom evi)
                                                        (sumarEv ev nom maxEv)
                                                        cantEv

--  Eficiencia: O(log N)
agregarEvPer :: Evidencia -> Nombre -> Map Nombre Persona -> Map Nombre Persona
agregarEvPer ev nom per = 
    assocM nom 
            (agregarEvidencia ev (fromJust(lookupM nom per))) per

--  Eficiencia: O(log N)
agregarPerEv :: Evidencia -> Nombre -> Map Evidencia [Nombre] -> Map Evidencia [Nombre]
agregarPerEv ev nom evi =
    case lookupM ev evi of
        (Just x) -> assocM ev (nom : x) evi
        Nothing  -> assocM ev [nom] evi

--  Eficiencia: O(N log N)
sumarEv :: Evidencia -> Nombre -> PriorityQueue Persona -> PriorityQueue Persona
sumarEv ev nom maxEv = 
    if (nombre (maxPQ maxEv)) == nom
        then insertPQ (agregarEvidencia ev (maxPQ maxEv)) (deleteMaxPQ maxEv)
        else insertPQ (maxPQ maxEv) (sumarEv ev nom (deleteMaxPQ maxEv))


-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
--Usuario Investigacion

--k)Propósito: Comienza una investigación con una lista de nombres sin evidencia
--  Eficiencia: O(N log N)
comenzarConPersonas :: [Nombre] -> Investigacion
comenzarConPersonas noms = ingresarPersonas noms (comenzarInvestigacion)

--l)Propósito: Indica si las personas en la investigación son todas inocentes
--  Eficiencia: O(N log N)
todosInocentes :: Investigacion -> Bool
todosInocentes inv = (lenght (posiblesInocentes inv)) == (lenght (nombresIngresados inv))

--m)Propósito: Indica si la evidencia en la lista es suficiente para cerrar el caso.
--  Eficiencia: O(N log N)
terminaCerrado :: [(Evidencia, Nombre)] -> Investigacion -> Bool
terminaCerrado [] inv     = casoCerrado inv
terminaCerrado (x:xs) inv = terminaCerrado xs (agregarPista (fst x) (snd x) inv)

--  Eficiencia: O(N log N)
agregarPista :: Evidencia -> Nombre -> Investigacion -> Investigacion
agregarPista ev nom inv = 
    if elem nom (nombresIngresados inv)
        then ingresarEvidencia ev nom inv
        else ingresarEvidencia ev nom (ingresarPersonas [nom] inv)

--n)
data Persona = ConsP Nombre [Evidencia] Int
