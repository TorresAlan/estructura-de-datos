
--La fiambrería


--Los siguientes tipos de datos permiten representar los sanguches de una fiambrería:

data Sanguche = Pan Relleno
                deriving (Show)

data Relleno = Feta TipoDeFeta Relleno
             | Aire
                deriving (Show)

data TipoDeFeta = Queso | Jamon | Mortadela | Salame
                deriving (Show)

--Ejemplo de Sanguche:
sanguche1 :: Sanguche
sanguche1 = Pan (Feta Queso (Feta Jamon(Aire)))

sanguche2 :: Sanguche
sanguche2 = Pan Aire

sangucheSoloJamon :: Sanguche
sangucheSoloJamon = Pan (Feta Jamon (Feta Jamon Aire))

--Resolver las siguientes funciones utilizando recursión estructural:

--a)Propósito: Dado un sanguche, indica si el relleno es solo de aire.
rellenoDeAire :: Sanguche -> Bool
rellenoDeAire (Pan rell) = esAire rell

--Proposito: Dado un relleno, devuelve true si es de Aire
esAire :: Relleno -> Bool
esAire Aire = True
esAire (Feta _ _) = False

--b)Propósito: Dado un sanguche, indica si solo tiene fetas de jamon.
esTortitaDeJamon :: Sanguche -> Bool
esTortitaDeJamon (Pan Aire) = False
esTortitaDeJamon (Pan rell) = rellenoSoloJamon rell

--Proposito: Dado un Relleno, indica si solo son rellenos de Jamon.
rellenoSoloJamon :: Relleno -> Bool
rellenoSoloJamon Aire = True
rellenoSoloJamon (Feta tf rell) = esJamon tf && (rellenoSoloJamon rell)

--Proposito: Dado un TipoDeFeta, indica si es de Jamon.
esJamon :: TipoDeFeta -> Bool
esJamon Jamon = True
esJamon _ = False

--c)Propósito: Dados un número n y un tipo de feta, agrega n fetas de ese tipo,
-- al principio del relleno del sanguche dado.
--Precondicion: n no debe ser un numero negativo.
mandaleNDe :: Int -> TipoDeFeta -> Sanguche -> Sanguche
mandaleNDe 0 tf (Pan rell) = Pan rell
mandaleNDe n tf (Pan rell) = mandaleNDe (n - 1) tf (Pan (Feta tf rell))

--d)Propósito: Quita todo el queso del relleno al sanguche dado.
peroSinQueso :: Sanguche -> Sanguche
peroSinQueso (Pan rell) = Pan (rellenosSinQueso rell)

--Proposito: Dado un relleno, devuelve ese relleno pero sin queso.
rellenosSinQueso :: Relleno -> Relleno
rellenosSinQueso Aire = Aire
rellenosSinQueso (Feta tf rell) = 
   if esQueso tf 
      then rellenosSinQueso rell
      else Feta tf (rellenosSinQueso rell)

--Proposito: Dado un TipoDeFeta, indica si es Queso.
esQueso :: TipoDeFeta -> Bool
esQueso Queso = True
esQueso _ = False

--e)Propósito: Devuelve una lista de fetas del relleno del sanguche, junto con su cantidad de apariciones.
ordenadosPorCantidad :: Sanguche -> [(TipoDeFeta, Int)]
ordenadosPorCantidad (Pan rell) = contarLosRellenos rell

--Proposito: Dado un relleno, devuelve una lista con todos los rellenos y sus cantidades.
contarLosRellenos :: Relleno -> [(TipoDeFeta, Int)]
contarLosRellenos Aire = []
contarLosRellenos (Feta tf rell) = sumarFeta tf (contarLosRellenos rell)

--Proposito: Dado un tipoDeFeta y una lista de tipos de feta y cantidades, agrega la feta o suma si ya esta en la lista.
sumarFeta :: TipoDeFeta -> [(TipoDeFeta, Int)] -> [(TipoDeFeta, Int)]
sumarFeta tf [] = [(tf, 1)]
sumarFeta tf (x:xs) = 
   if mismoTipoFeta tf (fst x)
      then (tf, 1 + snd x) : xs
      else x : sumarFeta tf xs

--Proposito: Dadas dos tiposDeFeta, denota si estas son iguales.
mismoTipoFeta :: TipoDeFeta -> TipoDeFeta -> Bool
mismoTipoFeta Queso Queso = True
mismoTipoFeta Jamon Jamon = True
mismoTipoFeta Mortadela Mortadela = True
mismoTipoFeta Salame Salame = True
mismoTipoFeta _ _ = False





--El Laberinto


--Los siguientes tipos de datos permiten representar un laberinto con bifurcaciones, cofres cerrados por llaves 
--y salidas:

-- Las llaves son simples números.
type Llave = Int

-- Existen direcciones que indican caminos dentro del laberinto.
data Dir = Izq | Der
            deriving Show

-- Los cofres tienen una cantidad de oro,
-- pero solo accesible para aquellos que posean las llaves que lo abren.
-- Pueden existir cofres que no requieran de llaves.
data Cofre = Cofre [Llave] Int
            deriving Show

-- Un laberinto posee salidas, celdas y bifurcaciones.
-- En las celdas hay cofres, en las salidas no hay nada,
-- y en las bifurcaciones puedo ir hacia uno u otro lado.
data Laberinto = Salida
               | Celda Cofre
               | Bifurcacion Laberinto Laberinto
                  deriving Show

--Ejemplo de Laberinto:
laberinto1 :: Laberinto
laberinto1 = Bifurcacion (Bifurcacion (Salida)(Celda cofre1)) (Salida)

laberinto2 :: Laberinto
laberinto2 = Salida

laberinto3 :: Laberinto
laberinto3 = Bifurcacion laberinto1 (Celda cofre2)

--Ejemplo de Cofre:
cofre1 :: Cofre
cofre1 = Cofre [2,3] 40

cofre2 :: Cofre
cofre2 = Cofre [3,4] 60


--Resolver las siguientes funciones utilizando recursión estructural:

--a)Propósito: Indica cuantas salidas posee un laberinto.
cantidadDeSalidas :: Laberinto -> Int
cantidadDeSalidas Salida = 1
cantidadDeSalidas (Celda _) = 0 
cantidadDeSalidas (Bifurcacion l1 l2) = cantidadDeSalidas l1 + cantidadDeSalidas l2

--b)Propósito: Dado un laberinto indica qué llaves se deben tener para poder abrir todos sus cofres.
--Nota: el resultado no debe tener llaves repetidas.
queLlavesDeboTener :: Laberinto -> [Llave]
queLlavesDeboTener Salida = []
queLlavesDeboTener (Celda cof) = lasLlavesDe cof
queLlavesDeboTener (Bifurcacion l1 l2) = sinLlavesRepetidas (queLlavesDeboTener l1 ++ queLlavesDeboTener l2)

--Proposito: Dado un cofre, devuelve su lista de llaves.
lasLlavesDe :: Cofre -> [Llave]
lasLlavesDe (Cofre ll _) = ll

--Proposito: Dada una lista de llaves, devuelve la lista eliminando los repetidos.
sinLlavesRepetidas :: [Llave] -> [Llave]
sinLlavesRepetidas [] = []
sinLlavesRepetidas (l:ls) = if notElem l ls
                            then l : sinLlavesRepetidas ls
                            else sinLlavesRepetidas ls

--c)Propósito: Indica cuánto oro puede conseguirse dada una lista de llaves.
cantidadDeOroCon :: [Llave] -> Laberinto -> Int
cantidadDeOroCon ll Salida = 0
cantidadDeOroCon ll (Bifurcacion l1 l2) = cantidadDeOroCon ll l1 + cantidadDeOroCon ll l2
cantidadDeOroCon ll (Celda cof) = 
   if puedoAbrirlo ll (lasLlavesDe cof)
      then oroDe cof
      else 0

--Proposito: Dado un cofre, devuelve la cantidad de oro que posee.
oroDe :: Cofre -> Int
oroDe (Cofre ll oro) = oro

--Proposito: Dadas dos listas de llave, indica si todos los elementos de la segunda existen en la primera.
puedoAbrirlo :: [Llave] -> [Llave] -> Bool
puedoAbrirlo [] ll' = False
puedoAbrirlo ll [] = True
puedoAbrirlo (x:xs) ll' = 
   if elem x ll'
      then puedoAbrirlo (x:xs) (quitarLLave x ll')
      else puedoAbrirlo xs ll'

--Proposito: Dada una llave y una lista de llaves, quita la llave de la lista.
--Precondicion: La llave dada debe existir en la lista.
quitarLLave :: Llave -> [Llave] -> [Llave]
quitarLLave ll (x:xs) = 
   if ll == x
      then xs
      else quitarLLave ll xs

--d)Propósito: Dada una lista de direcciones indica si llevan a una posible salida del laberinto.
haySalidaPor :: [Dir] -> Laberinto -> Bool
haySalidaPor dir Salida = True
haySalidaPor [] _ = False
haySalidaPor dir (Celda _) = False
haySalidaPor (x:xs) (Bifurcacion l1 l2) =
    if esIzq x
       then haySalidaPor xs l1
       else haySalidaPor xs l2

--Proposito: Dada una direccion, indica si es Izquierda.
esIzq :: Dir -> Bool
esIzq Izq = True
esIzq _ = False

--e)Propósito: Indica el camino a la salida más cercana.
--Precondición: Existe al menos una salida.
salidaMasCercana :: Laberinto -> [Dir]
salidaMasCercana Salida = []
salidaMasCercana (Bifurcacion l1 l2) = 
   if existeSalida l1 && existeSalida l2
      then if pasosHastaSalida l1 < pasosHastaSalida l2
               then Izq : salidaMasCercana l1
               else Der : salidaMasCercana l2
      else if existeSalida l1 
               then Izq : salidaMasCercana l1
               else Der : salidaMasCercana l2 

--Proposito: Dado un Laberinto, devuelve la cantidad de pasos hasta la salida.
--Precondicion: Existe al menos una salida.
pasosHastaSalida :: Laberinto -> Int
pasosHastaSalida Salida = 0
pasosHastaSalida (Bifurcacion l1 l2) =
    if haySalidaPor [Izq] (Bifurcacion l1 l2)
       then 1 + pasosHastaSalida l1
       else 1 + pasosHastaSalida l2 

--Proposito: Dado un Laberinto, dice si existe alguna salida.
existeSalida :: Laberinto -> Bool
existeSalida Salida = True
existeSalida (Celda _) = False
existeSalida (Bifurcacion l1 l2) = existeSalida l1 || existeSalida l2


