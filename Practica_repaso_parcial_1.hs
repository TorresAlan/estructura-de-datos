data Pizza = Prepizza
           | Capa Ingrediente Pizza

data Ingrediente = Salsa
                 | Queso
                 | Jamon
                 | Aceituna Int

cantidadDeCapas :: Pizza -> Int
cantidadDeCapas Prepizza = 0
cantidadDeCapas (Capa ing ps) = 1 + cantidadDeCapas ps

ingredientes :: Pizza -> [Ingrediente]
ingredientes Prepizza = []
ingredientes (Capa ing ps) = ing : ingredientes ps

soloQueso :: Pizza -> Bool
soloQueso Prepizza = True
soloQueso (Capa ing ps) = (esQueso ing) && (soloQueso ps) 

esQueso :: Ingrediente -> Bool
esQueso Queso = True
esQueso _ = False

soloAceitunas :: Pizza -> Bool
soloAceitunas Prepizza = True
soloAceitunas (Capa ing ps) = esAceituna ing && soloAceitunas ps

esAceituna :: Ingrediente -> Bool
esAceituna (Aceituna _) = True
esAceituna _ = False

sacarAceitunas :: Pizza -> Pizza
sacarAceitunas Prepizza = Prepizza
sacarAceitunas (Capa ing ps) = if esAceituna ing
                                    then sacarAceitunas ps
                                    else Capa ing (sacarAceitunas ps)

cuantasCapasTieneJamon :: Pizza -> Int
cuantasCapasTieneJamon Prepizza = 0
cuantasCapasTieneJamon (Capa ing ps) = if esJamon ing
                                            then 1 + cuantasCapasTieneJamon ps
                                            else cuantasCapasTieneJamon ps

esJamon :: Ingrediente -> Bool
esJamon Jamon = True
esJamon _ = False

cantIngNoAceituna :: Pizza -> [(Ingrediente, Int)]
cantIngNoAceituna Prepizza = []
cantIngNoAceituna (Capa ing ps) = sumarIng ing (cantIngNoAceituna ps)

sumarIng :: Ingrediente -> [(Ingrediente, Int)] -> [(Ingrediente, Int)]
sumarIng ing [] = [(ing, 1)]
sumarIng ing (x:xs) = if ingredientesIguales ing (fst x)
                        then (ing, 1 + snd x) : xs
                        else x : sumarIng ing xs

ingredientesIguales :: Ingrediente -> Ingrediente -> Bool
ingredientesIguales Queso Queso = True
ingredientesIguales Jamon Jamon = True
ingredientesIguales Salsa Salsa = True
ingredientesIguales _ _ = False

-- Dice si una pizza tiene salsa y queso
tieneSoloSalsaYQueso :: Pizza -> Bool
tieneSoloSalsaYQueso pizza = 
  tieneTodosPrimerosYSoloLosSegundos pizza [Salsa, Queso] [Salsa, Queso]
  
tieneTodosPrimerosYSoloLosSegundos :: Pizza -> [Ingrediente] -> [Ingrediente] -> Bool
tieneTodosPrimerosYSoloLosSegundos Prepizza requeridos permitidos = null requeridos
tieneTodosPrimerosYSoloLosSegundos (Capa ing resto) requeridos permitidos =
  perteneceIngrediente ing permitidos && 
  (tieneTodosPrimerosYSoloLosSegundos resto (sacar ing requeridos) permitidos)

sacar :: Ingrediente -> [Ingrediente] -> [Ingrediente]
sacar _ [] = [] 
sacar ingredienteASacar (i:is) = 
  if (ingredientesIguales i ingredienteASacar)
    then sacar ingredienteASacar is
    else i : (sacar ingredienteASacar is)

perteneceIngrediente  :: Ingrediente -> [Ingrediente] -> Bool
perteneceIngrediente _ [] = False
perteneceIngrediente buscado (i:is) = 
  ingredientesIguales i buscado || perteneceIngrediente buscado is







data Dir = Izq | Der

data Objeto = Tesoro | Chatarra

data Cofre = Cofre [Objeto]

data Mapa = Fin Cofre
          | Bifurcacion Cofre Mapa Mapa

hayTesoro :: Mapa -> Bool
hayTesoro (Fin c) = tieneTesoro c
hayTesoro (Bifurcacion c m1 m2) = tieneTesoro c || hayTesoro m1 || hayTesoro m2

tieneTesoro :: Cofre -> Bool
tieneTesoro (Cofre objs) = algunoEsTesoro objs

algunoEsTesoro :: [Objeto] -> Bool
algunoEsTesoro [] = False
algunoEsTesoro (o:os) = esTesoro o || algunoEsTesoro os 

esTesoro :: Objeto -> Bool
esTesoro Tesoro = True
esTesoro _ = False

cantChatarra :: Mapa -> Int
cantChatarra (Fin c) = cantChatarraEnCofre c
cantChatarra (Bifurcacion c m1 m2) = cantChatarraEnCofre c + cantChatarra m1 + cantChatarra m2

cantChatarraEnCofre :: Cofre -> Int
cantChatarraEnCofre (Cofre objs) = cuantoObjChatarra objs

cuantoObjChatarra :: [Objeto] -> Int
cuantoObjChatarra [] = 0
cuantoObjChatarra (x:xs) = if esChatarra x
                            then 1 + cuantoObjChatarra xs
                            else cuantoObjChatarra xs

esChatarra :: Objeto -> Bool
esChatarra Chatarra = True
esChatarra _ = False

-- Devuelve un mapa igual pero sin chatarra
sinChatarra :: Mapa -> Mapa
sinChatarra (Fin c) = (Fin (sacarChatarraCofre c))
sinChatarra (Bifurcacion c m1 m2) = (Bifurcacion (sacarChatarraCofre c) (sinChatarra m1) (sinChatarra m2))

sacarChatarraCofre :: Cofre -> Cofre
sacarChatarraCofre (Cofre objs) = (Cofre (sacarChatarra objs))

sacarChatarra :: [Objeto] -> [Objeto]
sacarChatarra [] = []
sacarChatarra (x:xs) = if esChatarra x
                            then sacarChatarra xs
                            else x : sacarChatarra xs

cosasCofre :: Cofre -> [Objeto]
cosasCofre (Cofre objs) = objs

-- Devuelve los objetos de todos los cofres
-- Nota: con repetidos
objetos :: Mapa -> [Objeto]
objetos (Fin c) = cosasCofre c
objetos (Bifurcacion c m1 m2) = cosasCofre c ++ objetos m1 ++ objetos m2

cantCharraEn :: [Dir] -> Mapa -> Int
cantCharraEn [] (Fin c) = cantChatarraEnCofre c
cantCharraEn [] (Bifurcacion c m1 m2) = cantChatarraEnCofre c
cantCharraEn (d:ds) (Fin c) = error "se termino el camino"
cantCharraEn (d:ds) (Bifurcacion c m1 m2) = if esIzq d
                                                then cantCharraEn ds m1
                                                else cantCharraEn ds m2

esIzq :: Dir -> Bool
esIzq Izq = True
esIzq _ = False

-- Indica si al final del camino hay un tesoro. 
-- Nota: el final de un camino se representa con una
-- lista vacía de direcciones.
hayTesoroEn :: [Dir] -> Mapa -> Bool
hayTesoroEn [] (Fin c) = 
  tieneTesoro c
hayTesoroEn [] (Bifurcacion c _ _) = 
  tieneTesoro c
hayTesoroEn (d:ds) (Fin c) = 
  error "me diste indicaciones invalidas"
hayTesoroEn (d:ds) (Bifurcacion c mi md) = 
  case d of
    Izq -> hayTesoroEn ds mi
    Der -> hayTesoroEn ds md









type Presa = String
type Territorio = String
type Nombre = String

data Lobo = Cazador Nombre [Presa] Lobo Lobo Lobo
          | Explorador Nombre [Territorio] Lobo Lobo
          | Cria Nombre

cantCazadores :: Lobo -> Int
cantCazadores (Cria n) = 0
cantCazadores (Cazador n ps l1 l2 l3) = 1 + cantCazadores l1 + cantCazadores l2 + cantCazadores l3
cantCazadores (Explorador n ts l1 l2) = cantCazadores l1 + cantCazadores l2

territorios :: Lobo -> [Territorio]
territorios (Cria n) = []
territorios (Cazador n ps l1 l2 l3) = territorios l1 ++ territorios l2 ++ territorios l3
territorios (Explorador n ts l1 l2) = ts ++ territorios l1 ++ territorios l2

cuantasCriasTieneN :: Nombre -> Lobo -> Int
cuantasCriasTieneN nom (Cria n) = 0
cuantasCriasTieneN nom (Cazador n ps l1 l2 l3) = if nom == n 
                                                    then cantCrias l1 + cantCrias l2 + cantCrias l3
                                                    else cuantasCriasTieneN nom l1 + cuantasCriasTieneN nom l2 + cuantasCriasTieneN nom l3
cuantasCriasTieneN nom (Explorador n ts l1 l2) = if nom == n
                                                    then cantCrias l1 + cantCrias l2
                                                    else cuantasCriasTieneN nom l1 + cuantasCriasTieneN nom l2

cantCrias :: Lobo -> Int
cantCrias (Cria n) = 1
cantCrias (Cazador n ps l1 l2 l3) = cantCrias l1 + cantCrias l2 + cantCrias l3
cantCrias (Explorador n ts l1 l2) = cantCrias l1 + cantCrias l2




