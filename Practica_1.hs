--Números enteros

--1. Defina las siguientes funciones:
--a)Dado un número devuelve su sucesor
sucesor :: Int -> Int
sucesor tipo = tipo + 1

--b)Dados dos números devuelve su suma utilizando la operación +.
sumar :: Int -> Int -> Int
sumar num1 num2 = num1 + num2

--c)Dado dos números, devuelve un par donde la primera componente es la división del
--primero por el segundo, y la segunda componente es el resto de dicha división. Nota:
--para obtener el resto de la división utilizar la función mod :: Int -> Int -> Int,
--provista por Haskell.
divisionYResto :: Int -> Int -> (Int, Int)
divisionYResto num1 num2 = ((div num1 num2), (mod num1 num2))

--d)Dado un par de números devuelve el mayor de estos.
maxDelPar :: (Int,Int) -> Int
maxDelPar (num1, num2) = if num1 > num2
                            then num1
                            else num2

--Solucion sin if
esMayor :: Int -> Int -> Bool
esMayor num1 num2 = num1 > num2 

elMayor :: Int -> Int -> Bool -> Int
elMayor num1 num2 True = num1
elMayor num1 num2 False = num2

maxDelParSinIf :: (Int,Int) -> Int
maxDelParSinIf (num1,num2) = elMayor num1 num2 (esMayor num1 num2)



--Tipos enumerativos
--1. Definir el tipo de dato Dir, con las alternativas Norte, Sur, Este y Oeste. Luego implementar
--las siguientes funciones:

data Dir = Norte | Sur | Este | Oeste 
     deriving Show

--a)Dada una dirección devuelve su opuesta.
opuesto :: Dir -> Dir
opuesto Norte = Sur
opuesto Sur = Norte
opuesto Este = Oeste
opuesto Oeste = Este

--b)Dadas dos direcciones, indica si son la misma. Nota: utilizar pattern matching y no ==.
iguales :: Dir -> Dir -> Bool
iguales Norte Norte = True
iguales Sur Sur = True
iguales Este Este = True
iguales Oeste Oeste = True
iguales _ _ = False 

--c)Dada una dirección devuelve su siguiente, en sentido horario, y suponiendo que no existe
--la siguiente dirección a Oeste. ¿Posee una precondición esta función? ¿Es una función
--total o parcial? ¿Por qué?
siguiente :: Dir -> Dir
siguiente Norte = Este
siguiente Este = Sur
siguiente Sur = Oeste
siguiente Oeste = error "No existe el siguiente de Oeste"

-- es una funcion parcial y la precondicion es que no existe el siguiente de Oeste

--2.Definir el tipo de dato DiaDeSemana, con las alternativas Lunes, Martes, Miércoles, Jueves,
--Viernes, Sabado y Domingo. Supongamos que el primer día de la semana es lunes, y el último
--es domingo. Luego implementar las siguientes funciones:

data DiaDeSemana = Lunes | Martes | Miercoles | Jueves | Viernes | Sabado | Domingo
        deriving Show

--a)Devuelve un par donde la primera componente es el primer día de la semana, y la
--segunda componente es el último día de la semana.
primeroYUltimoDia :: (DiaDeSemana, DiaDeSemana)
primeroYUltimoDia = (Lunes, Domingo)

--b)Dado un dia de la semana indica si comienza con la letra M.
empiezaConM :: DiaDeSemana -> Bool
empiezaConM Martes = True
empiezaConM Miercoles = True
empiezaConM _ = False

--c)Dado dos dias de semana, indica si el primero viene después que el segundo.
vieneDespues :: DiaDeSemana -> DiaDeSemana -> Bool
vieneDespues Martes Lunes = True
vieneDespues Miercoles Martes = True
vieneDespues Jueves Miercoles = True
vieneDespues Viernes Jueves = True
vieneDespues Sabado Viernes = True
vieneDespues Domingo Sabado = True
vieneDespues Lunes Domingo = True
vieneDespues _ _ = False

--d)Dado un dia de la semana indica si no es ni el primer ni el ultimo dia.
estaEnElMedio :: DiaDeSemana -> Bool
estaEnElMedio Lunes = False
estaEnElMedio Domingo = False
estaEnElMedio _ = True


--3.Los booleanos también son un tipo de enumerativo. Un booleano es True o False. Defina
--las siguientes funciones utilizando pattern matching (no usar las funciones sobre booleanos
--ya definidas en Haskell):

--a)Dado un booleano, si es True devuelve False, y si es False devuelve True.
--En Haskell ya está definida como not.
negar :: Bool -> Bool
negar True = False
negar False = True

--b)Dados dos booleanos, si el primero es True y el segundo es False, devuelve False, sino
--devuelve True.
--Nota: no viene implementada en Haskell.
implica :: Bool -> Bool -> Bool
implica True False = False
implica _ _ = True

--c)Dados dos booleanos si ambos son True devuelve True, sino devuelve False.
--En Haskell ya está definida como &&.
and :: Bool -> Bool -> Bool
and True True = True
and _ _ = False

--d)Dados dos booleanos si alguno de ellos es True devuelve True, sino devuelve False.
--En Haskell ya está definida como ||.
or :: Bool -> Bool -> Bool
or True _ = True
or _ True = True
or _ _ = False


--3. Registros

--1. Definir el tipo de dato Persona, como un nombre y la edad de la persona. Realizar las
--siguientes funciones:

data Persona = ConsP String Int
    deriving Show

--a)Devuelve el nombre de una persona
nombre :: Persona -> String
nombre (ConsP nombre edad) = nombre

--b)Devuelve la edad de una persona
edad :: Persona -> Int
edad (ConsP nombre edad) = edad

--c)Aumenta en uno la edad de la persona.
crecer :: Persona -> Persona
crecer (ConsP nombre edad) = ConsP nombre (edad + 1)

--d)Dados un nombre y una persona, devuelve una persona con la edad de la persona y el
--nuevo nombre.
cambioDeNombre :: String -> Persona -> Persona
cambioDeNombre nombreN (ConsP nombreV edad) = ConsP nombreN edad

--e)Dadas dos personas indica si la primera es mayor que la segunda.
esMayorQueLaOtra :: Persona -> Persona -> Bool
esMayorQueLaOtra (ConsP nombre1 edad1) (ConsP nombre2 edad2) = edad1 > edad2

--f)Dadas dos personas devuelve a la persona que sea mayor.
laQueEsMayor :: Persona -> Persona -> Persona
laQueEsMayor persona1 persona2 = laPersonaMayorEdad persona1 persona2 (esPersonaMayorQue persona1 persona2)

--Devuelve True si la primera persona es mayor que la segunda
esPersonaMayorQue :: Persona -> Persona -> Bool
esPersonaMayorQue (ConsP nombre1 edad1) (ConsP nombre2 edad2) = edad1 > edad2

--Si es True devuelve la primer persona, si es False devuelve la segunda
laPersonaMayorEdad :: Persona -> Persona -> Bool -> Persona
laPersonaMayorEdad persona1 persona2 True = persona1
laPersonaMayorEdad persona1 persona2 False = persona2


--2.Definir los tipos de datos Pokemon, como un TipoDePokemon (agua, fuego o planta) y un
--porcentaje de energía; y Entrenador, como un nombre y dos pokemones. Luego definir las
--siguientes funciones:

data TipoDePokemon = Fuego | Agua | Planta | Electrico
     deriving Show

data Pokemon = ConsPoke Int TipoDePokemon
     deriving Show

data Entrenador = ConsEn String Pokemon Pokemon 
     deriving Show

--a)Dados dos pokémon indica si el primero, en base al tipo, es superior al segundo. Agua
--supera a fuego, fuego a planta y planta a agua. Y cualquier otro caso es falso.
superaA :: TipoDePokemon -> TipoDePokemon
superaA Agua      = Fuego
superaA Planta    = Agua
superaA Fuego     = Planta
superaA Electrico = Agua

--b)Devuelve la cantidad de pokémon de determinado tipo que posee el entrenador.
sumaSiEsDeTipo :: TipoDePokemon -> Pokemon -> Int
sumaSiEsDeTipo Agua (ConsPoke energia Agua) = 1 
sumaSiEsDeTipo Planta (ConsPoke energia Planta) = 1 
sumaSiEsDeTipo Fuego (ConsPoke energia Fuego) = 1 
sumaSiEsDeTipo Electrico (ConsPoke energia Electrico) = 1 
sumaSiEsDeTipo _ _ = 0

cantidadDePokemonesDe :: TipoDePokemon -> Entrenador -> Int
cantidadDePokemonesDe tipo (ConsEn nombreEn poke1 poke2) = 
    sumaSiEsDeTipo tipo poke1 + sumaSiEsDeTipo tipo poke2


--c)Dado un par de entrenadores, devuelve a sus pokemones en una lista.
juntarPokemones :: (Entrenador, Entrenador) -> [Pokemon]
juntarPokemones ((ConsEn nombre1 poke1 poke2),(ConsEn nombre2 poke3 poke4)) = [poke1, poke2, poke3, poke4]



--4. Funciones polimórficas

--1. Defina las siguientes funciones polimórficas:

--a)Dado un elemento de algún tipo devuelve ese mismo elemento.
loMismo :: a -> a
loMismo a = a

--b)Dado un elemento de algún tipo devuelve el número 7.
siempreSiete :: a -> Int
siempreSiete a = 7

--Dadas una tupla, invierte sus componentes.
-- ¿Por qué existen dos variables de tipo diferentes?
swap :: (a,b) -> (b, a)
swap (a,b) = (b,a)

--Existen dos variables de tipo diferente para que esta funcion siga funcionando si los elementos
--de la tupla son de tipos distintos

--2. Responda la siguiente pregunta: ¿Por qué estas funciones son polimórficas?
--son polimorficas porque funcion sin importar el tipo de dato que use


--5. Pattern matching sobre listas

--1. Defina las siguientes funciones polimórficas utilizando pattern matching sobre listas (no
--utilizar las funciones que ya vienen con Haskell):

--a)Dada una lista de elementos, si es vacía devuelve True, sino devuelve False.
--Definida en Haskell como null.
estaVacia :: [a] -> Bool
estaVacia [] = True
estaVacia _ = False

--b)Dada una lista devuelve su primer elemento.
--Definida en Haskell como head.
--Nota: tener en cuenta que el constructor de listas es :
elPrimero :: [a] -> a
elPrimero [] = error "la lista no tiene elementos"
elPrimero (x:xs) = x 

--c)Dada una lista devuelve esa lista menos el primer elemento.
--Definida en Haskell como tail.
--Nota: tener en cuenta que el constructor de listas es :
sinElPrimero :: [a] -> [a]
sinElPrimero [] = error "la lista no tiene elementos"
sinElPrimero (x:xs) = xs

--d)Dada una lista devuelve un par, donde la primera componente es el primer elemento de la
--lista, y la segunda componente es esa lista pero sin el primero.
--Nota: tener en cuenta que el constructor de listas es :
splitHead :: [a] -> (a, [a])
splitHead [] = error "la lista no tiene elementos"
splitHead a = ((elPrimero a),(sinElPrimero a))