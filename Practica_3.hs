-- Tipos recursivos simples

-- 1.1. Celdas con bolitas

-- Representaremos una celda con bolitas de colores rojas y azules, de la siguiente manera:
data Color = Azul | Rojo
            deriving Show
data Celda = Bolita Color Celda | CeldaVacia
            deriving Show

-- En dicha representación, la cantidad de apariciones de un determinado color denota la cantidad
-- de bolitas de ese color en la celda. Por ejemplo, una celda con 2 bolitas azules y 2 rojas, podría
-- ser la siguiente:
celda1 = Bolita Rojo (Bolita Azul (Bolita Rojo (Bolita Azul CeldaVacia)))
celda2 = CeldaVacia
celda3 = Bolita Rojo (CeldaVacia)

-- Implementar las siguientes funciones sobre celdas:

-- Dados un color y una celda, indica la cantidad de bolitas de ese color. Nota: pensar si ya
-- existe una operación sobre listas que ayude a resolver el problema.
nroBolitas :: Color -> Celda -> Int
nroBolitas col CeldaVacia         = 0
nroBolitas col (Bolita color cel) = if esMismoColor col color 
                                        then 1 + nroBolitas col cel
                                        else nroBolitas col cel

-- Proposito: Toma dos colores y devuelve True si son iguales
esMismoColor :: Color -> Color -> Bool
esMismoColor Azul Azul = True
esMismoColor Rojo Rojo = True
esMismoColor _ _ = False

-- Dado un color y una celda, agrega una bolita de dicho color a la celda.
poner :: Color -> Celda -> Celda
poner col cel = (Bolita col cel)

-- Dado un color y una celda, quita una bolita de dicho color de la celda. Nota: a diferencia de
-- Gobstones, esta función es total.
sacar :: Color -> Celda -> Celda
sacar col CeldaVacia = CeldaVacia
sacar col (Bolita color cel) = if esMismoColor col color
                                    then cel
                                    else Bolita color (sacar col cel)

-- Dado un número n, un color c, y una celda, agrega n bolitas de color c a la celda.
ponerN :: Int -> Color -> Celda -> Celda
ponerN 0 col cel = cel
ponerN num col cel = ponerN (num -1) col (poner col cel)


-- 1.2. Camino hacia el tesoro

-- Tenemos los siguientes tipos de datos
data Objeto = Cacharro | Tesoro
                deriving Show
data Camino = Fin | Cofre [Objeto] Camino | Nada Camino
                deriving Show

mapa1 = Nada (Nada(Cofre [Cacharro, Cacharro] (Nada (Cofre [Cacharro, Tesoro] Fin))))
mapa2 = Cofre [Tesoro] Fin
mapa3 = Cofre [Tesoro] (Nada (Cofre [Tesoro] (Fin)))
mapa4 = Cofre [Tesoro] (Cofre [Tesoro] (Cofre [Tesoro] (Cofre [Tesoro] (Fin))))

-- Definir las siguientes funciones:

-- Indica si hay un cofre con un tesoro en el camino.
hayTesoro :: Camino -> Bool
hayTesoro cam = unTesoro (cofres cam)

-- Proposito: Dada una lista de objetos y devuelve True si alguno es un tesoro.
unTesoro :: [Objeto] -> Bool
unTesoro [] = False
unTesoro (x:xs) = (esTesoro x) || (unTesoro xs)

-- Proposito: Dado un objeto devuelve True si es un tesoro.
esTesoro :: Objeto -> Bool
esTesoro Tesoro = True
esTesoro _ = False

-- Proposito: Dado un camino devuelve una lista con los objetos de todos sus cofres.
cofres :: Camino -> [Objeto]
cofres Fin = []
cofres (Cofre obj cam) = obj ++ cofres cam
cofres (Nada cam) = cofres cam

-- Indica la cantidad de pasos que hay que recorrer hasta llegar al primer cofre con un tesoro.
-- Si un cofre con un tesoro está al principio del camino, la cantidad de pasos a recorrer es 0.
-- Precondición: tiene que haber al menos un tesoro.
pasosHastaTesoro :: Camino -> Int
pasosHastaTesoro Fin = error "No hay tesoros"
pasosHastaTesoro (Nada cam) = 1 + pasosHastaTesoro cam
pasosHastaTesoro (Cofre obj cam) = if ((unTesoro obj) == True)
                                        then 0
                                        else 1 + pasosHastaTesoro cam

-- Indica si hay un tesoro en una cierta cantidad exacta de pasos. Por ejemplo, si el número de
-- pasos es 5, indica si hay un tesoro en 5 pasos.
hayTesoroEn :: Int -> Camino -> Bool
hayTesoroEn pasos cam = existe pasos (todosLosPasosHastaTesoro cam)

existe :: Int -> [Int] -> Bool
existe a [] = False
existe a (x:xs) = if a == x
                    then True
                    else existe a xs

-- Proposito: Dado un Camino, devuelve una lista con todos sus pasos hasta el tesoro.
todosLosPasosHastaTesoro :: Camino -> [Int]
todosLosPasosHastaTesoro cam = if ((quedanTesoros cam) == True)
                                    then (pasosHastaTesoro cam) : todosLosPasosHastaTesoro (quitarPrimerTesoro cam)
                                    else []

-- Proposito: Dado un Camino, devuelve el camino sin el primer tesoro.
quitarPrimerTesoro :: Camino -> Camino
quitarPrimerTesoro Fin = Fin
quitarPrimerTesoro (Nada cam) = (Nada (quitarPrimerTesoro cam))
quitarPrimerTesoro (Cofre obj cam) = if ((unTesoro obj) == True)
                                        then (Cofre (quitarTesoro obj) cam)
                                        else (Cofre obj (quitarPrimerTesoro cam))

-- Proposito: Dada una lista de objetos, quita todos los tesoros de esta.
quitarTesoro :: [Objeto] -> [Objeto]
quitarTesoro [] = []
quitarTesoro (x:xs) = if ((esTesoro x) == True)
                        then quitarTesoro xs
                        else x : quitarTesoro xs

-- Proposito: Dado un camino, devuelve True si queda algun tesoro en este.
quedanTesoros :: Camino -> Bool
quedanTesoros Fin = False
quedanTesoros (Nada cam) = quedanTesoros cam
quedanTesoros (Cofre obj cam) = if ((unTesoro obj) == True)
                                    then True
                                    else quedanTesoros cam

-- Indica si hay al menos “n” tesoros en el camino.
alMenosNTesoros :: Int -> Camino -> Bool
alMenosNTesoros n cam = (n <= (cantTesoros cam))

-- Proposito: Dado un camino, devuelve la cantidad de tesoros en este.
cantTesoros :: Camino -> Int
cantTesoros Fin = 0
cantTesoros (Nada cam) = cantTesoros cam
cantTesoros (Cofre obj cam) = if unTesoro obj
                                    then 1 + cantTesoros cam
                                    else cantTesoros cam

-- Dado un rango de pasos, indica la cantidad de tesoros que hay en ese rango. Por ejemplo, si
-- el rango es 3 y 5, indica la cantidad de tesoros que hay entre hacer 3 pasos y hacer 5. Están
-- incluidos tanto 3 como 5 en el resultado.
-- cantTesorosEntre :: Int -> Int -> Camino -> Int
-- cantTesorosEntre inicio fin cam = cantTesoros (tomarNCaminos (fin - inicio) (quitarNCaminos (inicio) cam))

-- quitarNCaminos :: Int -> Camino -> Camino
-- quitarNCaminos 0 cam = cam
-- quitarNCaminos cant Fin = error "no hay suficiente camino"
-- quitarNCaminos cant cam = quitarNCaminos (cant - 1) (quitarCamino cam)

-- quitarCamino :: Camino -> Camino
-- quitarCamino Fin = error "no hay camino"
-- quitarCamino (Nada cam) = cam
-- quitarCamino (Cofre obj cam) = cam

-- tomarNCaminos :: Int -> Camino -> Camino
-- tomarNCaminos 0 cam = Fin
-- tomarNCaminos cant Fin = error "No hay suficiente camino"
-- tomarNCaminos cant (Nada cam) = (Nada (tomarNCaminos (cant - 1) (quitarCamino cam)))
-- tomarNCaminos cant (Cofre obj cam) = (Cofre obj (tomarNCaminos (cant - 1) (quitarCamino cam)))

-- 2. Tipos arbóreos
-- 2.1. Árboles binarios
-- Dada esta definición para árboles binarios
-- data Tree a = EmptyT | NodeT a (Tree a) (Tree a)
-- defina las siguientes funciones utilizando recursión estructural según corresponda:
-- 1. sumarT :: Tree Int -> Int
-- Dado un árbol binario de enteros devuelve la suma entre sus elementos.
-- 2. sizeT :: Tree a -> Int
-- Dado un árbol binario devuelve su cantidad de elementos, es decir, el tamaño del árbol (size
-- en inglés).
-- 3. mapDobleT :: Tree Int -> Tree Int
-- Dado un árbol de enteros devuelve un árbol con el doble de cada número.
-- 4. perteneceT :: Eq a => a -> Tree a -> Bool
-- Dados un elemento y un árbol binario devuelve True si existe un elemento igual a ese en el
-- árbol.
-- 5. aparicionesT :: Eq a => a -> Tree a -> Int
-- Dados un elemento e y un árbol binario devuelve la cantidad de elementos del árbol que son
-- iguales a e.
-- 6. leaves :: Tree a -> [a]
-- Dado un árbol devuelve los elementos que se encuentran en sus hojas.

-- Página 2 de 3

-- Estructuras de datos - UNQ

-- 7. heightT :: Tree a -> Int
-- Dado un árbol devuelve su altura.
-- Nota: la altura de un árbol (height en inglés), también llamada profundidad, es la cantidad
-- de niveles del árbol1

-- . La altura de un árbol vacío es cero y la de una hoja también.

-- 8. mirrorT :: Tree a -> Tree a
-- Dado un árbol devuelve el árbol resultante de intercambiar el hijo izquierdo con el derecho,
-- en cada nodo del árbol.
-- 9. toList :: Tree a -> [a]
-- Dado un árbol devuelve una lista que representa el resultado de recorrerlo en modo in-order.
-- Nota: En el modo in-order primero se procesan los elementos del hijo izquierdo, luego la raiz
-- y luego los elementos del hijo derecho.
-- 10. levelN :: Int -> Tree a -> [a]
-- Dados un número n y un árbol devuelve una lista con los nodos de nivel n. El nivel de un
-- nodo es la distancia que hay de la raíz hasta él. La distancia de la raiz a sí misma es 0, y la
-- distancia de la raiz a uno de sus hijos es 1.
-- Nota: El primer nivel de un árbol (su raíz) es 0.
-- 11. listPerLevel :: Tree a -> [[a]]
-- Dado un árbol devuelve una lista de listas en la que cada elemento representa un nivel de
-- dicho árbol.
-- 12. ramaMasLarga :: Tree a -> [a]
-- Devuelve los elementos de la rama más larga del árbol
-- 13. todosLosCaminos :: Tree a -> [[a]]
-- Dado un árbol devuelve todos los caminos, es decir, los caminos desde la raiz hasta las hojas.
-- 2.2. Expresiones Lógicas
-- El tipo algebraico ExpL modela expresiones lógicas de la siguiente manera:
-- data ExpL = Valor Bool
-- | And ExpB ExpB
-- | Or ExpB ExpB
-- | Not ExpB
-- Implementar las siguientes funciones utilizando el esquema de recursión estructural sobre Exp:
-- 1. evalL :: ExpL -> -> Bool
-- Dada una expresión lógica, y una lista de asignaciones de variables, devuelve el resultado
-- booleano de evaluarla. Precondición: todas las variables están asignadas.
-- 2. simplificarL :: ExpL -> ExpL
-- Dada una expresión lógica, la simplifica según los siguientes criterios (descriptos utilizando
-- notación matemática convencional):
-- a) False || x = x || False = x
-- b) True || x = x || True = True
-- c) True && x = x && True = x
-- d) False && x = x && False = False