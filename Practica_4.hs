--1. Pizzas

--Tenemos los siguientes tipos de datos:

data Pizza = Prepizza
           | Capa Ingrediente Pizza
           deriving (Show)

data Ingrediente = Salsa
                 | Queso
                 | Jamon
                 | Aceituna Int
                 deriving (Show)

--Ejemplos:
ingParaPizza = [Salsa, Queso, Jamon, (Aceituna 8)]

pizza1 = Capa Salsa 
                (Capa Queso 
                    (Capa Jamon 
                        (Capa (Aceituna 8) 
                            (Prepizza))))

pizza2 = Capa Queso
                (Capa Salsa(
                    Capa Queso (
                        Capa Salsa (
                            Prepizza))))

pizza3 = Capa Queso
                (Prepizza)

--Definir las siguientes funciones:

--Dada una pizza devuelve la lista de capas de ingredientes.
cantidadDeCapas :: Pizza -> Int
cantidadDeCapas Prepizza = 0
cantidadDeCapas (Capa ing ps) = 1 + cantidadDeCapas ps

--Dada una lista de ingredientes construye una pizza.
armarPizza :: [Ingrediente] -> Pizza
armarPizza [] = Prepizza
armarPizza (x:xs) = Capa x (armarPizza xs)

--Le saca los ingredientes que sean jamón a la pizza.
sacarJamon :: Pizza -> Pizza
sacarJamon Prepizza = Prepizza
sacarJamon (Capa ing ps) = 
    if esJamon ing
        then sacarJamon ps
        else Capa ing (sacarJamon ps)

--Dice si una pizza tiene salsa y queso.
tieneSoloSalsaYQueso :: Pizza -> Bool
tieneSoloSalsaYQueso pizza = 
  tieneTodosPrimerosYSoloLosSegundos pizza [Salsa, Queso] [Salsa, Queso]
  
tieneTodosPrimerosYSoloLosSegundos :: Pizza -> [Ingrediente] -> [Ingrediente] -> Bool
tieneTodosPrimerosYSoloLosSegundos Prepizza requeridos permitidos = null requeridos
tieneTodosPrimerosYSoloLosSegundos (Capa ing resto) requeridos permitidos =
  perteneceIngrediente ing permitidos && 
  (tieneTodosPrimerosYSoloLosSegundos resto (sacar ing requeridos) permitidos)

sacar :: Ingrediente -> [Ingrediente] -> [Ingrediente]
sacar _ [] = [] 
sacar ingredienteASacar (i:is) = 
  if (ingredientesIguales i ingredienteASacar)
    then sacar ingredienteASacar is
    else i : (sacar ingredienteASacar is)

perteneceIngrediente  :: Ingrediente -> [Ingrediente] -> Bool
perteneceIngrediente _ [] = False
perteneceIngrediente buscado (i:is) = 
  ingredientesIguales i buscado || perteneceIngrediente buscado is


--Recorre cada ingrediente y si es aceitunas duplica su cantidad.
duplicarAceitunas :: Pizza -> Pizza
duplicarAceitunas Prepizza = Prepizza
duplicarAceitunas (Capa ing ps) = 
    if esAceituna ing
        then Capa (dobleAceituna ing) (duplicarAceitunas ps)
        else Capa ing (duplicarAceitunas ps)

--Dada una cantidad de aceitunas devuelve el doble.
dobleAceituna :: Ingrediente -> Ingrediente
dobleAceituna (Aceituna x) = Aceituna (x * 2)

--Dada una lista de pizzas devuelve un par donde la primera componente es la cantidad de
--ingredientes de la pizza, y la respectiva pizza como segunda componente.
cantCapasPorPizza :: [Pizza] -> [(Int, Pizza)]
cantCapasPorPizza [] = []
cantCapasPorPizza (x:xs) = (cantCapas x, x) : cantCapasPorPizza xs

--Dada una pizza devuelve la cantidad de capas que la componen.
cantCapas :: Pizza -> Int
cantCapas Prepizza = 0
cantCapas (Capa ing ps) = 1 + cantCapas ps

--Dada una pizza devuelve una lista de sus ingredientes.
ingredientes :: Pizza -> [Ingrediente]
ingredientes Prepizza = []
ingredientes (Capa ing ps) = ing : ingredientes ps

--Dada una pizza devuelve True si es solo de queso.
soloQueso :: Pizza -> Bool
soloQueso Prepizza = True
soloQueso (Capa ing ps) = (esQueso ing) && (soloQueso ps) 

--Dado un ingrediente devuelve True si es queso.
esQueso :: Ingrediente -> Bool
esQueso Queso = True
esQueso _ = False

--Dado un ingrediente devuelve True si es salsa.
esSalsa :: Ingrediente -> Bool
esSalsa Salsa = True
esSalsa _     = False

--Dada una pizza devuelve True si es solo de aceitunas.
soloAceitunas :: Pizza -> Bool
soloAceitunas Prepizza = True
soloAceitunas (Capa ing ps) = esAceituna ing && soloAceitunas ps

--Dado un ingrediente devuelve True si es aceituna.
esAceituna :: Ingrediente -> Bool
esAceituna (Aceituna _) = True
esAceituna _ = False

--Dada una pizza le quita todas las aceitunas.
sacarAceitunas :: Pizza -> Pizza
sacarAceitunas Prepizza = Prepizza
sacarAceitunas (Capa ing ps) = if esAceituna ing
                                    then sacarAceitunas ps
                                    else Capa ing (sacarAceitunas ps)

--Dada una pizza devuelve cuantas capas de jamon tiene.
cuantasCapasTieneJamon :: Pizza -> Int
cuantasCapasTieneJamon Prepizza = 0
cuantasCapasTieneJamon (Capa ing ps) = if esJamon ing
                                            then 1 + cuantasCapasTieneJamon ps
                                            else cuantasCapasTieneJamon ps

--Dado un ingrediente devuelve True si es jamon.
esJamon :: Ingrediente -> Bool
esJamon Jamon = True
esJamon _ = False

--Dada una pizza devuelve una lista con ingredientes y cantidades de estos sin contar las aceitunas.
cantIngNoAceituna :: Pizza -> [(Ingrediente, Int)]
cantIngNoAceituna Prepizza = []
cantIngNoAceituna (Capa ing ps) = sumarIng ing (cantIngNoAceituna ps)

--Dado un ingrediente y una lista de ingredientes y cantidades, lo agrega o suma si ya esta en la lista.
sumarIng :: Ingrediente -> [(Ingrediente, Int)] -> [(Ingrediente, Int)]
sumarIng ing [] = [(ing, 1)]
sumarIng ing (x:xs) = if ingredientesIguales ing (fst x)
                        then (ing, 1 + snd x) : xs
                        else x : sumarIng ing xs

--Dados dos ingredientes devuelve True si son iguales.
ingredientesIguales :: Ingrediente -> Ingrediente -> Bool
ingredientesIguales Queso Queso = True
ingredientesIguales Jamon Jamon = True
ingredientesIguales Salsa Salsa = True
ingredientesIguales _ _ = False



--2. Mapa de tesoros (con bifurcaciones)

--Un mapa de tesoros es un árbol con bifurcaciones que terminan en cofres. Cada bifurcación y
--cada cofre tiene un objeto, que puede ser chatarra o un tesoro.

data Dir = Izq | Der
            deriving (Show)

data Objeto = Tesoro | Chatarra
                deriving (Show)

data Cofre = Cofre [Objeto]
                deriving (Show)

data Mapa = Fin Cofre
          | Bifurcacion Cofre Mapa Mapa
            deriving (Show)

--Ejemplos

mapa1 :: Mapa
mapa1 = Fin (Cofre [Chatarra])

mapa2 :: Mapa
mapa2 = 
  Bifurcacion (Cofre [Chatarra])
              (Fin (Cofre [Tesoro]))
              (Fin (Cofre [Tesoro]))

mapa3 :: Mapa
mapa3 = Bifurcacion (Cofre [Chatarra]) 
                    mapa1
                    mapa1

mapa4 :: Mapa
mapa4 = Bifurcacion (Cofre [Tesoro])
                    mapa3
                    mapa3

mapa5 :: Mapa
mapa5 = Bifurcacion (Cofre [Chatarra]) 
                    mapa4
                    mapa3

mapa6 :: Mapa
mapa6 = Bifurcacion (Cofre [Chatarra])
                    mapa5
                    mapa1


--Definir las siguientes operaciones:

--Indica si hay un tesoro en alguna parte del mapa.
hayTesoro :: Mapa -> Bool
hayTesoro (Fin c) = tieneTesoro c
hayTesoro (Bifurcacion c m1 m2) = tieneTesoro c || hayTesoro m1 || hayTesoro m2

--Dado un cofre devuelve True si contiene algun tesoro.
tieneTesoro :: Cofre -> Bool
tieneTesoro (Cofre objs) = algunoEsTesoro objs

--Dado un objeto devuelve True si es un tesoro.
esTesoro :: Objeto -> Bool
esTesoro Tesoro = True
esTesoro _      = False

--Dada una lista de objetos, devuelve True si alguno es un tesoro.
algunoEsTesoro :: [Objeto] -> Bool
algunoEsTesoro [] = False
algunoEsTesoro (o:os) = esTesoro o || algunoEsTesoro os

--Indica si al final del camino hay un tesoro. Nota: el final de un camino se representa con una
--lista vacía de direcciones.
hayTesoroEn :: [Dir] -> Mapa -> Bool
hayTesoroEn [] (Fin cof) = tieneTesoro cof
hayTesoroEn [] (Bifurcacion cof m1 m2) = tieneTesoro cof
hayTesoroEn (d:ds) (Fin cof) = error "No hay mas camino"
hayTesoroEn (d:ds) (Bifurcacion cof m1 m2) = 
    case d of
        Izq -> hayTesoroEn ds m1
        Der -> hayTesoroEn ds m2

--Indica el camino al tesoro. Precondición: existe un tesoro y es único.
caminoAlTesoro :: Mapa -> [Dir]
caminoAlTesoro (Fin cof) = []
caminoAlTesoro (Bifurcacion cof m1 m2) = 
    if tieneTesoro cof
        then []
        else if hayTesoro m1
            then Izq : caminoAlTesoro m1
            else Der : caminoAlTesoro m2 

--Indica el camino de la rama más larga.
caminoDeLaRamaMasLarga :: Mapa -> [Dir]
caminoDeLaRamaMasLarga (Fin cof) = []
caminoDeLaRamaMasLarga (Bifurcacion cof m1 m2) = 
    laMasLarga (Izq : caminoDeLaRamaMasLarga m1) (Der : caminoDeLaRamaMasLarga m2)

--Dada dos listas de direcciones devuelve la mas larga.
laMasLarga :: [Dir] -> [Dir] -> [Dir]
laMasLarga l1 l2 = if (length l1) > (length l2)
                        then l1
                        else l2

--Devuelve los tesoros separados por nivel en el árbol.
tesorosPorNivel :: Mapa -> [[Objeto]]
tesorosPorNivel (Fin cof) = [(soloTesoros cof)]
tesorosPorNivel (Bifurcacion cof m1 m2) = (soloTesoros cof) : juntarTesoros (tesorosPorNivel m1) (tesorosPorNivel m2)

--Dado un cofre, devuelve una lista con sus tesoros
soloTesoros :: Cofre -> [Objeto]
soloTesoros (Cofre objs) = tesoros objs

--Dada una lista de objetos, devuelve una lista solo con los que son tesoros
tesoros :: [Objeto] -> [Objeto]
tesoros [] = []
tesoros (o:os) = if esTesoro o
                    then o : tesoros os
                    else tesoros os

juntarTesoros :: [[Objeto]] -> [[Objeto]] -> [[Objeto]]
juntarTesoros obj1 []   = obj1
juntarTesoros [] obj2   = obj2
juntarTesoros (o:os) (i:is) = (o ++ i) : juntarTesoros os is

--Devuelve todos lo caminos en el mapa.
todosLosCaminos :: Mapa -> [[Dir]]
todosLosCaminos (Fin c) = [[]]
todosLosCaminos (Bifurcacion c mi md) = 
   (prepend Izq (todosLosCaminos mi)) ++ (prepend Der (todosLosCaminos md))

prepend :: Dir -> [[Dir]] -> [[Dir]]
prepend d [] = []
prepend d (ds:dss) =
   (d:ds):(prepend d dss)


--3. Nave Espacial

--Modelaremos una Nave como un tipo algebraico, el cual nos permite construir una nave espacial,
--dividida en sectores, a los cuales podemos asignar tripulantes y componentes. La representación
--es la siguiente:

data Componente = LanzaTorpedos | Motor Int | Almacen [Barril]
                    deriving Show
data Barril = Comida | Oxigeno | Torpedo | Combustible
                    deriving (Show, Eq)

data Sector = S SectorId [Componente] [Tripulante]
                deriving Show
type SectorId = String
type Tripulante = String

data Tree a = EmptyT | NodeT a (Tree a) (Tree a)
                deriving Show
data Nave = N (Tree Sector)
                deriving Show

--Ejemplo de nave:

naveInterplanetaria :: Nave
naveInterplanetaria = N navesita

navesita :: Tree Sector
navesita = NodeT sectorA (NodeT sectorB EmptyT EmptyT) (NodeT sectorC EmptyT (NodeT sectorD EmptyT EmptyT))

sectorA :: Sector 
sectorA = S "A" [LanzaTorpedos, LanzaTorpedos, barrilDeTorpedos] ["Jose", "Lucas", "Julia", "Ana"]

sectorB :: Sector 
sectorB = S "B" [barrilDeComida, barrilDeOxigeno, Motor 23, Motor 3] ["Jose", "Lucas", "Nicolas"]

sectorC :: Sector
sectorC = S "C" [Motor 12] []

sectorD :: Sector
sectorD = S "D" [] []

barrilDeComida :: Componente
barrilDeComida = Almacen [Comida, Comida, Comida]

barrilDeOxigeno :: Componente
barrilDeOxigeno = Almacen [Oxigeno]

barrilDeTorpedos :: Componente
barrilDeTorpedos = Almacen [Torpedo]

barrilDeCombustible :: Componente
barrilDeCombustible = Almacen [Combustible]


--Implementar las siguientes funciones utilizando recursión estructural:

--1.Propósito: Devuelve todos los sectores de la nave.
sectores :: Nave -> [SectorId]
sectores (N t) = sectoresArbol t

--Dado un Tree de Sectores, devuelve una lista con los id de los sectores.
sectoresArbol :: Tree Sector -> [SectorId]
sectoresArbol EmptyT = []
sectoresArbol (NodeT s sec1 sec2) = 
    idDeSector s : (sectoresArbol sec1 ++ sectoresArbol sec2)

--Dado un sector, devuelve su SectorId.
idDeSector :: Sector -> SectorId
idDeSector (S id comp trip) = id


--2.Propósito: Devuelve la suma de poder de propulsión de todos los motores de la nave. Nota:
--el poder de propulsión es el número que acompaña al constructor de motores.
poderDePropulsion :: Nave -> Int
poderDePropulsion (N t) = propulsionArbol t

--Dado un arbol de Sector, devuelve la propulsion de sus motores.
propulsionArbol :: Tree Sector -> Int
propulsionArbol EmptyT = 0
propulsionArbol (NodeT s sec1 sec2) = propulsionSector s + propulsionArbol sec1 + propulsionArbol sec2

--Dado un sector, devuelve la propulsion de sus motores
propulsionSector :: Sector -> Int
propulsionSector (S id comp trip) = propMotores comp

--Dada una lista de componentes, devuelve la propulsion de los motores
propMotores :: [Componente] -> Int
propMotores [] = 0
propMotores (c:cs) = if esMotor c 
                        then propulsion c + propMotores cs
                        else propMotores cs

--Dado un componente, devuelve True si es un motor.
esMotor :: Componente -> Bool
esMotor (Motor _) = True
esMotor _ = False

--Dado un Motor, devuelve su propulsion.
propulsion :: Componente -> Int
propulsion (Motor f) = f


--3.Propósito: Devuelve todos los barriles de la nave.
barriles :: Nave -> [Barril]
barriles (N t) = barrilesArbol t

--Dado un Arbol de sectores, devuelve una lista con todos sus barriles.
barrilesArbol :: Tree Sector -> [Barril]
barrilesArbol EmptyT = []
barrilesArbol (NodeT s sec1 sec2) = barrilesSector s ++ (barrilesArbol sec1 ++ barrilesArbol sec2)

--Dado un Sector, devuelve una lista con todos sus barriles.
barrilesSector :: Sector -> [Barril]
barrilesSector (S id comp trip) = barrilesComp comp

--Dada una lista de componentes, devuelve una lista con todos los barriles en ellos.
barrilesComp :: [Componente] -> [Barril]
barrilesComp [] = []
barrilesComp (c:cs) = if esAlmacen c
                        then barrilesAlmacen c ++ barrilesComp cs
                        else barrilesComp cs

--Dado un componente, devuelve True si es un almacen.
esAlmacen :: Componente -> Bool
esAlmacen (Almacen _) = True
esAlmacen _ = False

--Dado un almacen, devuelve una lista de sus barriles.
barrilesAlmacen :: Componente -> [Barril]
barrilesAlmacen (Almacen bar) = bar


--Propósito: Añade una lista de componentes a un sector de la nave.
--Nota: ese sector puede no existir, en cuyo caso no añade componentes.
agregarASector :: [Componente] -> SectorId -> Nave -> Nave
agregarASector comp secId (N t) = N (agregarASectorArbol comp secId t )

--Dado una lista de componentes, un sectorid y un arbol de sectores, agrega esos componentes al sectorid dado.
agregarASectorArbol :: [Componente] -> SectorId -> Tree Sector -> Tree Sector
agregarASectorArbol comp secId EmptyT = EmptyT
agregarASectorArbol comp secId (NodeT s sec1 sec2) = NodeT (agregarComponentesSi comp secId s)
                                                            (agregarASectorArbol comp secId sec1)
                                                            (agregarASectorArbol comp secId sec2)

--Dada una lista de componentes, un sector id y un sector, agregar esos componentes al sector si el sectorid dado es
--igual al id del sector
agregarComponentesSi :: [Componente] -> SectorId -> Sector -> Sector
agregarComponentesSi comp secId sec = if (idDeSector sec) == secId
                                        then agregarComponentes comp sec
                                        else sec

--Dada una lista de componentes y un sector, agrega esos componentes al sector.
agregarComponentes :: [Componente] -> Sector -> Sector
agregarComponentes comp (S id compini trip) = S id (comp ++ compini) trip



--Propósito: Incorpora un tripulante a una lista de sectores de la nave.
--Precondición: Todos los id de la lista existen en la nave.
asignarTripulanteA :: Tripulante -> [SectorId] -> Nave -> Nave
asignarTripulanteA trip secid (N t) = N (asignarTripulanteArbol trip secid t)

--Dado un tripulante, una lista de sectorid, y un arbol de sectores, busca esos sectores en el arbol y agrega al tripulante
--en todos.
asignarTripulanteArbol :: Tripulante -> [SectorId] -> Tree Sector -> Tree Sector
asignarTripulanteArbol trip secid EmptyT = EmptyT
asignarTripulanteArbol trip [] tree = tree
asignarTripulanteArbol trip (i:is) (NodeT s sec1 sec2) =
     if i == (idDeSector s )
         then asignarTripulanteArbol trip is (NodeT (agregarTripulante trip s) sec1 sec2)
         else NodeT s (asignarTripulanteArbol trip (i:is) sec1) (asignarTripulanteArbol trip (i:is) sec2)

--Dado un tripulante y un sector, agrega al tripulante a dicho sector.
agregarTripulante :: Tripulante -> Sector -> Sector
agregarTripulante trip (S secId comp trips) = S secId comp (trip : trips)


--Propósito: Devuelve los sectores en donde aparece un tripulante dado.
sectoresAsignados :: Tripulante -> Nave -> [SectorId]
sectoresAsignados trip (N t) = sectoresAsignadosArbol trip t

--Dado un tripulante y un arbol de sectores, devuelve una lista con los sectorid de los sectores donde pertenece
-- el tripulante.
sectoresAsignadosArbol :: Tripulante -> Tree Sector -> [SectorId]
sectoresAsignadosArbol trip EmptyT = []
sectoresAsignadosArbol trip (NodeT s sec1 sec2) = 
    if estaAsignado trip s
        then idDeSector s : ((sectoresAsignadosArbol trip sec1) ++ (sectoresAsignadosArbol trip sec2))
        else (sectoresAsignadosArbol trip sec1) ++ (sectoresAsignadosArbol trip sec2)

--Dado un tripulante y un sector, devuelve True si el tripulante esta asignado a dicho sector.
estaAsignado :: Tripulante -> Sector -> Bool
estaAsignado trip (S secId comp trips) = elem trip trips

--Propósito: Devuelve la lista de tripulantes, sin elementos repetidos.
tripulantes :: Nave -> [Tripulante]
tripulantes (N t) = sinTripRepetidos (tripulantesArbol t)

tripulantesArbol :: Tree Sector -> [Tripulante]
tripulantesArbol EmptyT = []
tripulantesArbol (NodeT s sec1 sec2) = tripulantesDelSec s ++ (tripulantesArbol sec1 ++ tripulantesArbol sec2)

tripulantesDelSec :: Sector -> [Tripulante]
tripulantesDelSec (S secId comp trips) = trips

sinTripRepetidos :: [Tripulante] -> [Tripulante]
sinTripRepetidos [] = []
sinTripRepetidos (t:ts) = if notElem t ts
                            then t : sinTripRepetidos ts
                            else sinTripRepetidos ts


--4. Manada de lobos

--Modelaremos una manada de lobos, como un tipo Manada, que es un simple registro compuesto
--de una estructura llamada Lobo, que representa una jerarquía entre estos animales.
--Los diferentes casos de lobos que forman la jerarquía son los siguientes:
--Los cazadores poseen nombre, una lista de especies de presas cazadas y 3 lobos a cargo.
--Los exploradores poseen nombre, una lista de nombres de territorio explorado (nombres de
--bosques, ríos, etc.), y poseen 2 lobos a cargo.
--Las crías poseen sólo un nombre y no poseen lobos a cargo.
--La estructura es la siguiente:

type Presa = String -- nombre de presa
type Territorio = String -- nombre de territorio
type Nombre = String -- nombre de lobo
data Lobo = Cazador Nombre [Presa] Lobo Lobo Lobo
          | Explorador Nombre [Territorio] Lobo Lobo
          | Cria Nombre
    deriving Show

data Manada = M Lobo
    deriving Show

--1. Construir un valor de tipo Manada que posea 1 cazador, 2 exploradores y que el resto sean
--crías. Resolver las siguientes funciones utilizando recursión estructural sobre la estructura
--que corresponda en cada caso:

--Presas
cerdo :: Presa
cerdo = "Cerdo"

vaca :: Presa
vaca = "Vaca"

--Territorios
catan :: Territorio
catan = "Catan"

berazategui :: Territorio
berazategui = "Berazategui"

--Lobos
fenryr = Cazador "Fenryr" [cerdo, vaca, cerdo] hatti hattini wolf

hatti = Explorador "Hatti" [catan, berazategui] hattini wolf

wolf = Explorador "Wolf" [berazategui] hattini wolfy

hattini = Cria "Hattini"

wolfy = Cria "Wolfy"

--Manada
manada = M fenryr


--2.Propósito: dada una manada, indica si la cantidad de alimento cazado es mayor a la can-
--tidad de crías.
buenaCaza :: Manada -> Bool
buenaCaza (M l) = (cantCrias l) < (alimentoCazado (cazadores l))

--Dado un lobo, devuelve una lista con los nombres de todas las crias.
nombreCrias :: Lobo -> [Nombre]
nombreCrias (Cria n) = [n]
nombreCrias (Explorador n t l1 l2) = nombreCrias l1 ++ nombreCrias l2
nombreCrias (Cazador n p l1 l2 l3) = nombreCrias l1 ++ nombreCrias l2 ++ nombreCrias l3

--Dada un lobo, devuelve la cantidad de crias.
cantCrias :: Lobo -> Int
cantCrias l = length (sinRepetidos (nombreCrias l ))

--Dada una lista de nombres, devuelve la lista sin repetidos.
sinRepetidos :: [Nombre] -> [Nombre]
sinRepetidos [] = []
sinRepetidos (t:ts) = if notElem t ts
                            then t : sinRepetidos ts
                            else sinRepetidos ts

--Dado un lobo, me devuelve su nombre.
nombreLobo :: Lobo -> Nombre
nombreLobo (Cria n) = n
nombreLobo (Explorador n t l1 l2) = n
nombreLobo (Cazador n p l1 l2 l3) = n

--Dado un lobo, devuelve una lista con lobos cazadores.
cazadores :: Lobo -> [Lobo]
cazadores (Cria n) = []
cazadores (Explorador n t l1 l2) = cazadores l1 ++ cazadores l2
cazadores (Cazador n p l1 l2 l3) = (Cazador n p l1 l2 l3) : (cazadores l1 ++ cazadores l2 ++ cazadores l3)

--Dada una lista de lobos cazadores, devuelve la cantidad de alimento cazado.
alimentoCazado :: [Lobo] -> Int
alimentoCazado [] = 0
alimentoCazado (l:ls) =  length (presas l) + alimentoCazado ls

--Dado un lobo cazador, devuelve sus presas en una lista.
presas :: Lobo -> [Presa]
presas (Cazador n p l1 l2 l3) = p


--3.Propósito: dada una manada, devuelve el nombre del lobo con más presas cazadas, junto
--con su cantidad de presas. Nota: se considera que los exploradores y crías tienen cero presas
--cazadas, y que podrían formar parte del resultado si es que no existen cazadores con más de
--cero presas.
elAlfa :: Manada -> (Nombre, Int)
elAlfa (M l) = if null (cazadores l)
                    then (nombreLobo l, 0)
                    else ((nombreLobo (cazadorConMasPresas (cazadores l))), (cantPresas(cazadorConMasPresas (cazadores l))))

--Dada una lista de lobos cazadores, devuelve el lobo con mas presas cazadas.
cazadorConMasPresas :: [Lobo] -> Lobo
cazadorConMasPresas [l] = l
cazadorConMasPresas (l:ls) =
    if (cantPresas l)  > (cantPresas (head ls))
        then cazadorConMasPresas (l : (tail ls) )
        else cazadorConMasPresas ls

--Dado un lobo cazador, devuelve la cantidad de presas cazadas.
cantPresas :: Lobo -> Int
cantPresas (Cazador n p l1 l2 l3) = length p


--Propósito: dado un territorio y una manada, devuelve los nombres de los exploradores que
--pasaron por dicho territorio.
losQueExploraron :: Territorio -> Manada -> [Nombre]
losQueExploraron ter (M l) = sinNombresRepetidos (losQueExploraron' ter l)

losQueExploraron' :: Territorio -> Lobo -> [Nombre]
losQueExploraron' ter (Cria n) = []
losQueExploraron' ter (Explorador n t l1 l2) = 
    if elem ter t
        then n : ((losQueExploraron' ter l1) ++ (losQueExploraron' ter l2))
        else ((losQueExploraron' ter l1) ++ (losQueExploraron' ter l2))
losQueExploraron' ter (Cazador n p l1 l2 l3) = ((losQueExploraron' ter l1) ++ (losQueExploraron' ter l2) ++ (losQueExploraron' ter l3))

sinNombresRepetidos :: [Nombre] -> [Nombre]
sinNombresRepetidos [] = []
sinNombresRepetidos (t:ts) = if notElem t ts
                            then t : sinNombresRepetidos ts
                            else sinNombresRepetidos ts


--Propósito: dada una manada, denota la lista de los pares cuyo primer elemento es un terri-
--torio y cuyo segundo elemento es la lista de los nombres de los exploradores que exploraron
--dicho territorio. Los territorios no deben repetirse.
exploradoresPorTerritorio :: Manada -> [(Territorio, [Nombre])]
exploradoresPorTerritorio (M l) = exploradoresPorTerritorio' (M l) ( sinNombresRepetidos (todosLosTerritorios l))

exploradoresPorTerritorio' :: Manada -> [Territorio] -> [(Territorio, [Nombre])]
exploradoresPorTerritorio' _ [] = []
exploradoresPorTerritorio' m (t:ts) = (t, losQueExploraron t m) : (exploradoresPorTerritorio' m ts)

todosLosTerritorios :: Lobo -> [Territorio]
todosLosTerritorios (Cria n) = []
todosLosTerritorios (Cazador n p l1 l2 l3) = todosLosTerritorios l1 ++ todosLosTerritorios l2 ++ todosLosTerritorios l3
todosLosTerritorios (Explorador n t l1 l2) = t ++ (todosLosTerritorios l1 ++ todosLosTerritorios l2)


--Propósito: dado un nombre de cazador y una manada, indica el nombre de todos los
--cazadores que tienen como subordinado al cazador dado (directa o indirectamente).
--Precondición: hay un cazador con dicho nombre y es único.
superioresDelCazador :: Nombre -> Manada -> [Nombre]
superioresDelCazador n (M l) = superioresDelCazador' n l

superioresDelCazador' :: Nombre -> Lobo -> [Nombre]
superioresDelCazador' n (Explorador n' t l1 l2) = superioresDelCazador' n (dondeExisteLobo n [l1, l2])
superioresDelCazador' n (Cazador n' p l1 l2 l3) = 
    if n == n'
        then []
        else n' : superioresDelCazador' n (dondeExisteLobo n [l1, l2, l3])


dondeExisteLobo :: Nombre -> [Lobo] -> Lobo
dondeExisteLobo n (l:ls) = if estaLoboCazador n l
                                then l
                                else dondeExisteLobo n ls

estaLoboCazador :: Nombre -> Lobo -> Bool
estaLoboCazador n (Cria n' ) = False
estaLoboCazador n (Explorador n' t l1 l2) = estaLoboCazador n l1 || estaLoboCazador n l2
estaLoboCazador n (Cazador n' p l1 l2 l3) = 
    if n == n' 
        then True
        else estaLoboCazador n l1 || estaLoboCazador n l2 || estaLoboCazador n l3


