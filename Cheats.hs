pad :: [[Char]] -> [[Char]]
pad = zipWith (++) (repeat "   ")

arreglarT :: Show a => Tree a -> IO()
arreglarT EmptyT = putStrLn "-"

arreglarT tree = printRecursive (prettyprint_helper tree)
  where 
    printRecursive [] = putStrLn ""
    printRecursive (x:xs) = do
      putStrLn x
      printRecursive xs

prettyprint_helper (NodeT x left right)
  = (show x) : (prettyprint_subtree left right)
    where
      prettyprint_subtree left right =
        ((pad "+- " "|  ") (prettyprint_helper right))
          ++ ((pad "`- " "   ") (prettyprint_helper left))
      pad first rest = zipWith (++) (first : repeat rest)

prettyprint_helper EmptyT = [""]